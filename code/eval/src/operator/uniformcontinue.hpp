#include "operator.hpp"
#include "solution.h"

class UniformContinue : public Operator
{
public:
    UniformContinue(unsigned int _n) : n(_n) {}
    void operator()(Solution &solution)
    {
        solution.x.resize(n);
        for (unsigned i = 0; i < n; i++)
            solution.x[i] = 1.0;
    }

protected:
    unsigned int n;
};