#include "operator.hpp"
#include "solution.h"
#include <algorithm>
#include <random>

class RandomPermutation : public Operator
{
public:
    RandomPermutation(std::default_random_engine &_rng, unsigned int _n) : rng(_rng),
                                                                           n(_n) {}
    void operator()(Solution &solution)
    {
        // Initialisation de l'emplacement de chaque photo, dans l'ordre de celles-ci
        solution.sigma.resize(n);

        for (unsigned i = 0; i < n; i++)
            solution.sigma[i] = i;

        // Mélange de cette liste
        std::shuffle(std::begin(solution.sigma), std::end(solution.sigma), rng);
    }

protected:
    // attributs de la classe RandomPermutation
    std::default_random_engine &rng;
    unsigned int n;
};