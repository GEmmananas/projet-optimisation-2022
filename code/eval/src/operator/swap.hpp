#include "solution.h"

class Swap
{
public:
    Swap() {}
    void operator()(Solution &solution, size_t u, size_t v)
    {
        // size_t = type représentant un index dans un tableau
        size_t temp = solution.sigma[u];
        solution.sigma[u] = solution.sigma[v];
        solution.sigma[v] = temp;
    }
};