#ifndef __operator__
#define __operator__

#include <solution.h>

class Operator
{
public:
    virtual void operator()(Solution &_solution) = 0;
};
#endif
