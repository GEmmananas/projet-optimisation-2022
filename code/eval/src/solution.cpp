#include <iostream>
#include "solution.h"

std::ostream &operator<<(std::ostream &_os, Solution &s) {
	return s.printOn(_os);
}
