#ifndef __randomsearch__
#define __randomsearch__

#include <random>
#include "solution.h"
#include "posterEval.h"
#include "search.hpp"

class RandomSearch : public Search
{
public:
    RandomSearch(std::default_random_engine &_rng, PosterEval &_eval) : rng(_rng), eval(_eval) {}
    virtual void operator()(Solution &_solution);

protected:
    std::default_random_engine &rng;
    PosterEval &eval;
};
#endif
