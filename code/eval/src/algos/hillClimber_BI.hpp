#ifndef __hillclimber_bi__
#define __hillclimber_bi__

#include "solution.h"
#include "posterEval.h"
#include "search.hpp"

class HillClimberBestImprovementSearch : public Search
{
public:
    HillClimberBestImprovementSearch(PosterEval &_eval) : eval(_eval) {}
    virtual void operator()(Solution &_solution);

protected:
    PosterEval &eval;
};
#endif
