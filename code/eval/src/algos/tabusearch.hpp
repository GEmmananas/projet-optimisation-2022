#ifndef __tabusearch__
#define __tabusearch__

#include "solution.h"
#include "posterEval.h"
#include "search.hpp"

class TabuSearch : public Search
{
public:
    TabuSearch(PosterEval &_eval) : eval(_eval) {}
    virtual void operator()(Solution &_solution);

protected:
    PosterEval &eval;
};
#endif
