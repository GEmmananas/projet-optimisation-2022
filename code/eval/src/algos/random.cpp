#include <random>
#include "solution.h"
#include "random.hpp"
#include "posterEval.h"
#include "search.hpp"
#include "operator/randompermutation.hpp"
#include "operator/uniformcontinue.hpp"

void RandomSearch::operator()(Solution &_solution)
{
    RandomPermutation random(rng, _solution.sigma.size());
    UniformContinue uniform(_solution.x.size());
    Solution s(_solution.sigma.size());
    uniform(s);
    while (time(NULL) < timeLimit_)
    {
        random(s);
        eval(s);
        if (s.fitness < _solution.fitness)
            _solution = s;
    }
}
