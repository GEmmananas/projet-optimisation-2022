#include <random>
#include <map>
#include <tuple>
#include "solution.h"
#include "tabusearch.hpp"
#include "posterEval.h"
#include "search.hpp"
#include "operator/swap.hpp"
#include "operator/uniformcontinue.hpp"

#define TABU_PARAM 10

void TabuSearch::operator()(Solution &bestKnown)
{
    // memory contient un historique des derniers swap effectués sur la solution
    map<tuple<size_t, size_t>, int> memory;
    Swap swap;
    UniformContinue uniform(bestKnown.x.size());
    Solution s(bestKnown.sigma.size());
    uniform(s);

    // Donne une valeur par défaut à tous les emplacements des photos
    for (unsigned i = 0; i < s.sigma.size(); i++)
        s.sigma[i] = i;

    eval(s);

    while (time(NULL) < timeLimit_)
    {
        double bestFitness = s.fitness;
        size_t uBest = -1;
        size_t vBest = -1;

        double currentFitness = s.fitness;

        // Recherche du meilleur voisin
        for (size_t u = 1; u < s.sigma.size(); u++)
        {
            for (size_t v = 0; v < u; v++)
            {
                swap(s, u, v);
                eval(s);

                if ((memory[tuple<size_t, size_t>(u, v)] == 0 || bestKnown.fitness > s.fitness) && bestFitness > s.fitness)
                {
                    bestFitness = s.fitness;
                    uBest = u;
                    vBest = v;
                }

                swap(s, u, v);
            }
        }

        // Si on a trouvé un meilleur voisin, alors on fait un swap
        if (uBest >= 0)
        {
            swap(s, uBest, vBest);
            s.fitness = bestFitness;

            if (bestKnown.fitness > s.fitness)
                bestKnown = s;
        }
        else
        {
            s.fitness = currentFitness;
        }

        // Mise à jour de la mémoire
        for (size_t u = 1; u < s.sigma.size(); u++)
        {
            for (size_t v = 0; v < u; v++)
            {
                if (memory[tuple<size_t, size_t>(u, v)] > 0)
                    memory[tuple<size_t, size_t>(u, v)]--;
            }
        }

        if (uBest >= 0)
            memory[tuple<size_t, size_t>(uBest, vBest)] = TABU_PARAM;
    }
}
