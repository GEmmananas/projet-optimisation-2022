#include <random>
#include "solution.h"
#include "hillClimber_BI.hpp"
#include "posterEval.h"
#include "search.hpp"
#include "operator/swap.hpp"
#include "operator/uniformcontinue.hpp"

void HillClimberBestImprovementSearch::operator()(Solution &_solution)
{
    Swap swap;
    UniformContinue uniform(_solution.x.size());
    Solution s(_solution.sigma.size());
    uniform(s);

    // Donne une valeur par défaut à tous les emplacements des photos
    for (unsigned i = 0; i < s.sigma.size(); i++)
        s.sigma[i] = i;

    while (time(NULL) < timeLimit_)
    {
        for (size_t u = 1; u < s.sigma.size(); u++)
        {
            for (size_t v = 0; v < u; v++)
            {
                // u et v correspondent aux deux cases à inverser
                swap(s, u, v);
                eval(s);
                if (s.fitness < _solution.fitness)
                    _solution = s;
                else
                    // Restoration de la solution initialement reçue
                    swap(s, u, v);
            }
            // Si le temps de recherche est dépassé, on sort de la boucle
            if (time(NULL) >= timeLimit_)
            {
                break;
            }
        }
    }
}
