#ifndef __search__
#define __search__

#include "solution.h"

class Search
{
public:
    Search() {}
    virtual void operator()(Solution &_solution) {}
    virtual void timeLimit(time_t limit)
    {
        timeLimit_ = limit;
    }

protected:
    time_t timeLimit_;
};
#endif
